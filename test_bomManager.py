#!/usr/bin/env python3

from unittest import TestCase

from bom_manager import BomManager, Part, BomItem


class TestBomManager(TestCase):
    def setUp(self):
        self.bom = BomManager()


class TestAddParts(TestBomManager):
    def test_add_parts(self):
        pen_black_al = Part(name="Pen", attributes={"color": "black", "material": "aluminum"})
        pen_black_pl = Part(name="Pen", attributes={"color": "black", "material": "plastic"})
        pen_blue_al = Part(name="Pen", attributes={"color": "blue", "material": "aluminum"})
        pen_blue_pl = Part(name="Pen", attributes={"color": "blue", "material": "plastic"})

        self.bom.add_part(pen_black_al)
        self.bom.add_part(pen_black_pl)
        self.bom.add_part(pen_blue_al)
        self.bom.add_part(pen_blue_pl)

        self.assertTrue(len(self.bom.itemList) == 4)

        # Try adding the same part as a child of another
        self.assertRaises(Exception, lambda: self.bom.add_child_parts(pen_black_al, [pen_black_al]))

    def test_remove_parts(self):
        for item in self.bom.itemList:
            self.bom.delete_part(item.part)

        self.assertTrue(len(self.bom.itemList) == 0)

        # Delete a part that is a child of another
        parent_part = Part(name="parent")
        child_part = Part(name="child")
        self.bom.add_part(parent_part)
        self.bom.add_part(child_part)
        self.bom.add_child_parts(parent_part, [child_part])

        self.assertTrue(child_part in [child.part for child in parent_part.childrenParts])

        self.bom.delete_part(child_part)

        self.assertTrue(child_part not in [child.part for child in parent_part.childrenParts])

        # Add same part again
        self.bom.add_part(parent_part)
        self.assertTrue(self.bom.itemList[0].quantity == 2)

        # cleanup
        self.bom.delete_part(parent_part)


class TestExercise(TestBomManager):
    def test_exercise(self):
        pen_black_al = Part(name="Pen", attributes={"color": "black", "material": "aluminum"})
        pen_black_pl = Part(name="Pen", attributes={"color": "black", "material": "plastic"})
        pen_blue_al = Part(name="Pen", attributes={"color": "blue", "material": "aluminum"})
        pen_blue_pl = Part(name="Pen", attributes={"color": "blue", "material": "plastic"})

        pen_top_al = Part(name="pen_barrel_top", attributes={"material": "aluminum"})
        pen_bot_black_al = Part(name="pen_barrel_bot", attributes={"material": "aluminum", "color": "blue"})
        pen_bot_blue_al = Part(name="pen_barrel_bot", attributes={"material": "aluminum", "color": "blue"})

        pen_top_pl = Part(name="pen_barrel_top", attributes={"material": "plastic"})
        pen_bot_black_pl = Part(name="pen_barrel_bot", attributes={"material": "plastic", "color": "blue"})
        pen_bot_blue_pl = Part(name="pen_barrel_bot", attributes={"material": "plastic", "color": "blue"})

        pocket_clip = Part(name="pocket_clip", attributes={"material": "aluminum"})

        # Assemble Pen tops
        self.bom.add_child_parts(pen_top_al, [pocket_clip])
        self.bom.add_child_parts(pen_top_pl, [pocket_clip])

        # Other parts
        thruster = Part(name="thruster", attributes={"material": "plastic"})
        cam = Part(name="cam", attributes={"material": "plastic"})
        external_grip = Part(name="external_grip", attributes={"material": "rubber"})
        integrated_grip = Part(name="integrated_grip", attributes={"material": "rubber"})
        spring = Part(name="spring")

        # Assemble Ink cartridges
        ink_cartridge_blk = Part(name="ink_cartridge_black")
        ink_cartridge_blu = Part(name="ink_cartridge_blue")
        ink_cartridge_bdy = Part(name="ink_cartridge_body", attributes={"material": "plastic"})
        ink_cartridge_cap = Part(name="ink_cartridge_cap", attributes={"material": "plastic"})
        ink_cartridge_tip = Part(name="ink_cartridge_tip")

        ink_blk = Part("ink", attributes={"color": "black"})
        ink_blu = Part("ink", attributes={"color": "blue"})

        self.bom.add_child_parts(ink_cartridge_blk, [ink_cartridge_bdy, ink_cartridge_cap, ink_cartridge_tip, ink_blk])
        self.bom.add_child_parts(ink_cartridge_blu, [ink_cartridge_bdy, ink_cartridge_cap, ink_cartridge_tip, ink_blu])

        # Assemble pens
        self.bom.add_child_parts(pen_black_al, [pen_top_al, pen_bot_black_al, ink_cartridge_blk, thruster, cam, integrated_grip, spring])
        self.bom.add_child_parts(pen_black_pl, [pen_top_pl, pen_bot_black_pl, ink_cartridge_blk, thruster, cam, external_grip, spring])
        self.bom.add_child_parts(pen_blue_al, [pen_top_al, pen_bot_blue_al, ink_cartridge_blu, thruster, cam, integrated_grip, spring])
        self.bom.add_child_parts(pen_blue_pl, [pen_top_pl, pen_bot_blue_pl, ink_cartridge_blu, thruster, cam, external_grip, spring])

        # Create box parts and packaging
        pen_box_blk_al = Part("pen_box", attributes={"color": "black", "package": "Aluminum Pen"})
        pen_box_blk_pl = Part("pen_box", attributes={"color": "black", "package": "Plastic Pen"})
        pen_box_blu_al = Part("pen_box", attributes={"color": "blue", "package": "Aluminum Pen"})
        pen_box_blu_pl = Part("pen_box", attributes={"color": "blue", "package": "Plastic Pen"})

        pen_box_top_blk = Part("pen_box_top", attributes={"color": "black"})
        pen_box_top_blu = Part("pen_box_top", attributes={"color": "blue"})
        pen_box_bot_blk = Part("pen_box_bot", attributes={"color": "black"})
        pen_box_bot_blu = Part("pen_box_bot", attributes={"color": "blue"})

        pen_box_insert = Part("pen_box_insert")

        # Assemble Boxes
        self.bom.add_child_parts(pen_box_blk_al, [pen_box_top_blk, pen_box_bot_blk, pen_box_insert, pen_black_al])
        self.bom.add_child_parts(pen_box_blk_pl, [pen_box_top_blk, pen_box_bot_blk, pen_box_insert, pen_black_pl])
        self.bom.add_child_parts(pen_box_blu_al, [pen_box_top_blu, pen_box_bot_blu, pen_box_insert, pen_blue_al])
        self.bom.add_child_parts(pen_box_blu_pl, [pen_box_top_blu, pen_box_bot_blu, pen_box_insert, pen_blue_pl])

        top_assemblies = self.bom.get_top_assemblies()
        self.assertTrue(len(top_assemblies) == 4)

        # test orphans
        orphans = self.bom.get_all_orphans()
        self.assertTrue(len(orphans) == 0)
        orphanPart = Part("I AM AN ORPHAN")
        self.bom.add_part(orphanPart)
        orphans = self.bom.get_all_orphans()
        self.assertTrue(len(orphans) == 1)
