import copy
import threading
from typing import List
from multiprocessing import Value

counter = Value('i', 0)
counter_lock = threading.Lock()


def synchronized(func):
    func.__lock__ = threading.Lock()

    def synced_func(*args, **kws):
        with func.__lock__:
            return func(*args, **kws)

    return synced_func


def id_generator():
    with counter_lock:
        counter.value += 1
        return counter.value


class Part(object):
    """
    A part abstraction.

    Parts contain a mandatory name and have attributes in a dict.

    Parts may have children that are also parts.
    Parts do not reference their parent parts as this could create a cycle.

    """
    def __init__(self, name: str, attributes=None, childrenParts=None):
        self.name = name
        self.attributes = attributes if attributes else dict()
        self.childrenParts: List[BomItem] = childrenParts if childrenParts else []
        self.isAssembly = True if len(self.childrenParts) > 0 else False
        self.id = id_generator()

    def __eq__(self, other):
        if isinstance(other, Part):
            return (self.id == other.id) and (self.name == other.name) and (self.attributes == other.attributes)
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.__repr__())


class BomItem(object):
    """
    A simple bookkeeping class for handling multiple parts within an assembly or parts list
    """
    def __init__(self, part: Part, quantity: int):
        self.part = part
        self.quantity = quantity


class BomManager(object):
    """
    A class to manage a list of parts and their children.
    """
    def __init__(self):
        self.itemList: List[BomItem] = []

    @synchronized
    def get_part_from_id(self, partID: int):
        """
        Gives the known part from a given ID
        :param partID: ID of target part
        :return: Part from ID. None if not found
        """
        found_part = next((part for part in self.itemList if part.part.id == partID), None)
        return found_part

    @synchronized
    def get_part_from_attributes(self, part: Part):
        """
        Used to check if given part is the same as an existing one regardless of ID
        :param part:
        :return: Existing part
        """
        existingPart = next((existing for existing in self.itemList
                             if existing.part.attributes == part.attributes
                             and existing.part.name == part.name), None)
        return existingPart

    @synchronized
    def add_part(self, part: Part, quantity: int = 1):
        """
        Will add part to the BOM.

        Since the insertion always checks for existing, this is at least O(n)

        If a part already exists, the quantity given will be added to the Bom Item

        :param part:
        :param quantity: optionally add more than one part
        :return:
        """
        existingID = self.get_part_from_id(part.id)
        existingAttributes = self.get_part_from_attributes(part)
        targetPart = part
        if not existingID and not existingAttributes:
            self.itemList.append(BomItem(part, quantity))
        else:
            if existingAttributes:
                targetPart = existingAttributes
            targetPart.quantity += quantity

    @synchronized
    def add_child_parts(self, parent: Part, children: List[Part]):
        """
        Turns parent into an assembly and adds the child to the parts list.

        Since the insertion always checks for existing, this is at least O(n + c)
        where n is the number of parts in self.partList and
        where c is the number of children passed as an argument

        Parts cannot be children of the same part.

        :param parent: Part to become an assembly
        :param children: Dependent parts. May contain assemblies.
        :return: True on success. False when parent is not in parts list.
        """
        # Check for invalid Parts or assemblies that have itself as a child.
        if parent in children:
            raise Exception("Cannot add part as child of itself")
        parentInList = next((par for par in self.itemList if par.part == parent), None)
        if not parentInList:
            existingAttributes = self.get_part_from_attributes(parent)
            if existingAttributes:
                parentInList = existingAttributes
            else:
                self.add_part(parent)
                parentInList = next((par for par in self.itemList if par.part == parent), None)
        for x in set(children):
            self.add_part(x, children.count(x))
            parentInList.part.childrenParts.append(BomItem(x, children.count(x)))
        parentInList.part.isAssembly = True

    @synchronized
    def delete_part(self, part: Part):
        """
        Removes given part from part list and all assemblies
        :param part:
        :return: None
        """
        contain_part = self.find_parent_assembly(part)
        for parent in contain_part:
            for child in parent.part.childrenParts:
                if child.part == part:
                    parent.part.childrenParts.remove(child)
        partDel = next((item for item in self.itemList if item.part == part), None)
        self.itemList.remove(partDel)

    @synchronized
    def get_all_parts(self):
        """
        Get every part in the BOM
        :return:
        """
        return self.itemList

    @synchronized
    def get_all_assemblies(self):
        """
        Get every part in the BOM that is an assembly
        :return:
        """
        return [item for item in self.itemList if item.part.isAssembly]

    @synchronized
    def get_top_assemblies(self):
        """
        :return: all assemblies that are not children of another assembly
        """
        all_assemblies = self.get_all_assemblies()
        ret_all_assemblies = copy.copy(all_assemblies)
        for assemblyItem in all_assemblies:
            sub_assemblies = self.get_subassemblies(assemblyItem.part)
            for assem in sub_assemblies:
                partDel = next((item for item in self.itemList if item.part == assem.part), None)
                if partDel in ret_all_assemblies:
                    ret_all_assemblies.remove(partDel)
        return ret_all_assemblies

    @synchronized
    def get_all_subassemblies(self):
        """
        :return: all assemblies that are children of another assembly
        """
        all_assemblies = self.get_all_assemblies()
        sub_assemblies = []
        # This could be more pythonian.
        for assemblyItem in all_assemblies:
            for bom_item in assemblyItem.part.childrenParts:
                if bom_item.part.isAssembly:
                    partAdd = next((item for item in self.itemList if item.part == bom_item.part), None)
                    sub_assemblies.append(partAdd)
        return sub_assemblies

    @synchronized
    def get_subassemblies(self, part: Part):
        """
        Function to find all subassemblies of a given part (ignores given part as assembly)

        This function is recursive and can be improved.

        :param part:
        :return: a flat list of all subassemblies as Part objects
        """
        def __sub_part_get_assemblies(item: BomItem):
            sub_assemblies = []
            if item.part.isAssembly:
                sub_assemblies.append(item)
            # This did not  work as it would add empy lists to the list.
            # sub_assemblies += [__sub_part_get_assemblies(subAss) for subAss in part.childrenParts]
            possible_sub_assemblies = []
            for subAss in item.part.childrenParts:
                poss_sub_part_child_assemblies = __sub_part_get_assemblies(subAss)
                for item in poss_sub_part_child_assemblies:
                    possible_sub_assemblies.append(item)
            for item in possible_sub_assemblies:
                sub_assemblies.append(item)
            return sub_assemblies
        sub_assemblies = []
        for assem in part.childrenParts:
            child_sub_assemblies = __sub_part_get_assemblies(assem)
            if len(child_sub_assemblies) > 0:
                sub_assemblies.append(child_sub_assemblies)
        flat_list = [item for sublist in sub_assemblies for item in sublist]
        return flat_list

    @synchronized
    def get_all_component_parts(self):
        """
        Gives list of all parts that are not sub-assemblies or assemblies.
        :return: flat list of all BomItems
        """
        return [comp for comp in self.itemList if not comp.part.isAssembly]

    @synchronized
    def get_component_parts(self, part: Part):
        """
        Gives list of all parts of a given part that are not sub-assemblies
        :param part:
        :return: flat list of BomItems from given part
        """
        # We need to be sure to convert
        # Since we do not care about assemblies, we do not need recursion
        return [comp for comp in part.childrenParts if not comp.part.isAssembly]

    @synchronized
    def get_all_orphans(self):
        """
        Get all parts that do not have children and are not children of any other part
        :return: list of orphan parts
        """
        childless = [item for item in self.itemList if len(item.part.childrenParts) == 0]
        orphans = []
        for child in childless:
            isOrphan = True
            for item in self.itemList:
                test = any(sub.part == child.part for sub in item.part.childrenParts)
                if test:
                    isOrphan = False
                    break
            if isOrphan:
                orphans.append(child)
        return orphans

    @synchronized
    def get_first_level_children(self, part: Part):
        """
        Get the first level of children from a given part
        :param part: a part that is an assembly
        :return: List of BomItem
        """
        part_in_list = next((par for par in self.itemList if par.part == part), None)
        if not part_in_list:
            raise Exception("Part {} does not exist".format(part.name))
        if not part_in_list.part.isAssembly:
            raise Exception("Part {} is not an assembly".format(part.name))
        first_level = copy.copy([child for child in part_in_list.part.childrenParts])
        for child in first_level:
            child.part.childrenParts = []
        return first_level

    @synchronized
    def get_all_children(self, part: Part):
        """
        Get all children of a given part
        :param part:
        :return: list of BomItem
        """
        part_in_list = next((par for par in self.itemList if par.part == part), None)
        if not part_in_list:
            raise Exception("Part {} does not exist".format(part.name))
        return [item for item in part_in_list.part.childrenParts]

    @synchronized
    def find_parent_assembly(self, part: Part):
        """
        Return all assemblies that contain a specific given part, either directly or indirectly
        :param part:
        :return:
        """
        return [assembly for assembly in self.get_all_assemblies()
                for sub_part in assembly.part.childrenParts if part == sub_part.part]
