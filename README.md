# BomManager Challenge

### About

This project is a coding challenge to implement a *Bill Of Materials*(BOM) manager that is capable of handling 
a parts hierarchy with a RESTful API.

I've decided to use Python for this project to simplify portability as well as I have never used the Flask 
framework before.

| file               | note                                                             |
|--------------------|------------------------------------------------------------------|
| bom_manager.py     | Main library for all BOM actions                                 |
| bom_server.py      | RESTful server that extends the BOM library to a web API         |
| test_bomManager.py | unittest of bom_manager.py                                       |
| curlTests.sh       | Bash script that implements curl commands to test the bom_server |

The sample data of the assignment is located in `curlTests.sh`. This bash script exercises the RESTful API as well as creates two different models of pens with different colors of the model.

### Assignment
```
The assignment is to implement a JSON-based REST API to create, update, delete and query bills of materials. The implemented API should be addressable with an HTTP client like curl, wget or Postman. Functionality should be provided to:
- create a new part
- add one or more parts as "children" to a "parent" part, which then becomes an assembly
- remove one or more parts from an assembly
- delete a part (thereby also deleting the part from its parent assemblies)
- list all parts
- list all assemblies
- list all top level assemblies (assemblies that are not children of another assembly)
- list all subassemblies (assemblies that are children of another assembly)
- list all component parts (parts that are not subassemblies, but are included in a parent assembly)
- list all orphan parts (parts with neither parents nor children)
- list all the first-level children of a specific assembly
- list all children of a specific assembly
- list all parts in a specific assembly (which are not subassemblies)
- list all assemblies that contain a specific child part, either directly or indirectly (via a subassembly)

Provide code and associated sample data to instantiate and query several varieties of pen:
- two different models of pen (e.g. metal barrel & plastic barrel)
- two different colors of each model (e.g. red, blue, black)

As a bonus, describe and implement an enhancement of your choosing to improve robustness, performance and/or functionality of the service.
```

### Added features
`bom_manager.py` is thread safe. It is done so by using synchronization with a `Lock` from the built in `threading` library.

Queries were simplified by having a confirming returned payload for added parts that could then be 
used directly with assigning children.

### Requirements

Python 3.6+ is required to implement and test.

RESTful server was implemented with [Flask](https://palletsprojects.com/p/flask/)

### Setup

Python is assumed to be installed on testing system.

I highly recommend using `venv` as to not disturb your system or other development environments.

If you do not have `venv`:
```bash
pip install virtualenv
```

Create virtual environment (in the repo directory):
```bash
virtualenv -p python3.6 venv
source ./venv/bin/activate
pip install flask
```

### Usage

Running the server:
```bash
python ./bom_server.py
```

Running the RESTful test (requires the server to be running):
```bash
./curlTests.sh
```

Running the unittests:
```bash
./test_bomManager.py
```

#### Endpoints
```
- GET    /api                            returns this schema string
- POST   /api/add_part                   data={"name": "value",
                                               "quantity"[default=1]: <value>,
                                               "attributes"[optional]: {"key": "value", "key2": "value"}}
                                         return={"id": <partID>}
- POST   /api/add_child_parts            data={"parent": <parentPart>,
                                               "children": [<partA>, <partB>, etc.]}
- GET    /api/get_all_parts              data[optional]={"id": <partID>}
                                         return=[<partA>, <partB>, etc.]
- GET    /api/get_all_assemblies         return=[<partA>, <partB>, etc.]
- GET    /api/get_top_assemblies         return=[<partA>, <partB>, etc.]
- GET    /api/get_all_subassemblies      return=[<partA>, <partB>, etc.]
- GET    /api/get_all_components         return=[<partA>, <partB>, etc.]
- GET    /api/get_all_orphans            return=[<partA>, <partB>, etc.]
- GET    /api/get_first_level_children   data[optional]={"id": <partID>}
                                         return=[<partA>, <partB>, etc.]
- GET    /api/get_children               data[optional]={"id": <partID>}
                                         return=[<partA>, <partB>, etc.]
- GET    /api/find_parent_assembly       data={"id": <partID>}
                                         return=[<partA>, <partB>, etc.]


Part Structure:
{"name": "value",
"quantity"[default=1]: <value>,
"attributes"[optional]: {"key": "value", "key2": "value"}}
Or, for queries:
{"id": <partID>}
```

### Discussion

There are many areas this service can be imporved.

Notably, the data structure for `bom_manager` can be improved.
The current implementation attempted to avoid recursion where possible.
A more performant structure could be chosen depending on application..

The RESTful API could use some work. The actions here are also influenced by 
application. `add_part` and `add_child_parts` increment the quantity in the BOM. 
This may not be ideal in some circumstances as one may want to add a part and then 
assign that part as a child of a parent. In the total BOM, this would make an 
extra part. this behaviour is seen in `curlTests.sh`. It can be avoided by 
simply adding the full Part payload to the element in the `children` list.

