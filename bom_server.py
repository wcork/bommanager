#!/usr/bin/env python3

from flask import Flask, jsonify, request
from flask.json import JSONEncoder
from multiprocessing import Value, Lock

from bom_manager import BomManager, BomItem, Part


class BomItemJSONEncoder(JSONEncoder):
    """
    Encoder for BomManager.BomItem and BomManager.Part
    """
    def default(self, obj):
        if isinstance(obj, BomItem):
            return {
                'part': obj.part,
                'quantity': obj.quantity,
            }
        if isinstance(obj, Part):
            return {
                'name': obj.name,
                'attributes': obj.attributes,
                'children': obj.childrenParts,
            }
        return super(BomItemJSONEncoder, self).default(obj)


BOM_MANAGER = BomManager()

app = Flask(__name__)
app.json_encoder = BomItemJSONEncoder

help_message = """
API Usage:
- GET    /api                            returns this schema string
- POST   /api/add_part                   data={"name": "value",
                                               "quantity"[default=1]: <value>,
                                               "attributes"[optional]: {"key": "value", "key2": "value"}}
                                         return={"id": <partID>}
- POST   /api/add_child_parts            data={"parent": <parentPart>,
                                               "children": [<partA>, <partB>, etc.]}
- GET    /api/get_all_parts              data[optional]={"id": <partID>}
                                         return=[<partA>, <partB>, etc.]
- GET    /api/get_all_assemblies         return=[<partA>, <partB>, etc.]
- GET    /api/get_top_assemblies         return=[<partA>, <partB>, etc.]
- GET    /api/get_all_subassemblies      return=[<partA>, <partB>, etc.]
- GET    /api/get_all_components         return=[<partA>, <partB>, etc.]
- GET    /api/get_all_orphans            return=[<partA>, <partB>, etc.]
- GET    /api/get_first_level_children   data[optional]={"id": <partID>}
                                         return=[<partA>, <partB>, etc.]
- GET    /api/get_children               data[optional]={"id": <partID>}
                                         return=[<partA>, <partB>, etc.]
- GET    /api/find_parent_assembly       data={"id": <partID>}
                                         return=[<partA>, <partB>, etc.]


Part Structure:
{"name": "value",
"quantity"[default=1]: <value>,
"attributes"[optional]: {"key": "value", "key2": "value"}}
Or, for queries:
{"id": <partID>}


"""


def verify_payload_existing(payload):
    """
    :param payload: dict of a Part payload
    :return: Part from BOM that matches given payload
    """

    # We can first look for an ID
    target_part_id = payload.get("id")
    if not target_part_id:
        # we can check against the given part attributes
        if payload.get("name"):
            target_part = BOM_MANAGER.get_part_from_attributes(Part(payload.get("name"), payload.get("attributes", {})))
        else:
            raise Exception("Missing \'name\' field in data")
    else:
        target_part = BOM_MANAGER.get_part_from_id(target_part_id)
    return target_part


@app.route('/api', methods=['GET'])
def help():
    return help_message


@app.route('/api/add_part', methods=['POST'])
def add_part():
    try:
        payload = request.get_json()
        if not payload.get("name"):
            raise Exception("Missing \'name\' field in data")
        part_to_add = Part(payload.get("name"), payload.get("attributes", {}))
    except Exception as e:
        raise Exception("ERROR: Payload for \'add_part\' invalid: {}".format(e))
    try:
        existing_part = BOM_MANAGER.get_part_from_attributes(part_to_add)
        if existing_part:
            part_to_add = existing_part.part
        BOM_MANAGER.add_part(part_to_add, payload.get("quantity", 1))
    except Exception as e:
        raise Exception("Unable to add part to BOM: {}".format(e))
    return jsonify({"id": part_to_add.id})


@app.route('/api/add_child_parts', methods=['POST'])
def add_child_parts():
    try:
        payload = request.get_json()
        if not payload.get("parent"):
            raise Exception("Missing \'parent\' field in data")
        if not payload.get("children"):
            raise Exception("Missing \'children\' field in data")
        parent_part = verify_payload_existing(payload.get("parent")).part
        child_parts = []
        for child in payload.get("children"):
            child_to_add = verify_payload_existing(child)
            if child_to_add:
                child_parts.append(child_to_add.part)
            else:
                child_parts.append(Part(child.get("name"), child.get("attributes", {})))
    except Exception as e:
        raise Exception("ERROR: Payload for \'add_child_parts\' invalid: {}".format(e))
    try:
        BOM_MANAGER.add_child_parts(parent_part, child_parts)
    except Exception as e:
        raise Exception("Unable to add part to BOM: {}".format(e))
    childNames = [child.name for child in child_parts]
    return "Parts \"{}\" have been added to {} id: {}".format(",".join(childNames), parent_part.name, parent_part.id)


@app.route('/api/get_all_parts', methods=['GET'])
def get_all_parts():
    payload = request.get_json()
    if payload:
        # Client requesting all_parts for a specific assembly
        target_part = verify_payload_existing(payload)
        if not target_part:
            raise Exception("ERROR: Requested Part does not exist")
        parts = BOM_MANAGER.get_all_children(target_part.part)
        return jsonify(parts)
    else:
        all_parts = BOM_MANAGER.get_all_parts()
        return jsonify(all_parts)


@app.route('/api/get_all_assemblies', methods=['GET'])
def get_all_assemblies():
    all_assemblies = BOM_MANAGER.get_all_assemblies()
    return jsonify(all_assemblies)


@app.route('/api/get_top_assemblies', methods=['GET'])
def get_top_assemblies():
    top_assemblies = BOM_MANAGER.get_top_assemblies()
    return jsonify(top_assemblies)


@app.route('/api/get_all_subassemblies', methods=['GET'])
def get_all_subassemblies():
    all_subassemblies = BOM_MANAGER.get_all_subassemblies()
    return jsonify(all_subassemblies)


@app.route('/api/get_all_components', methods=['GET'])
def get_all_components():
    components = BOM_MANAGER.get_all_component_parts()
    return jsonify(components)


@app.route('/api/get_all_orphans', methods=['GET'])
def get_all_orphans():
    orphans = BOM_MANAGER.get_all_orphans()
    return jsonify(orphans)


@app.route('/api/get_first_level_children', methods=['GET'])
def get_first_level_children():
    try:
        payload = request.get_json()
        target_part = verify_payload_existing(payload)
    except Exception as e:
        raise Exception("ERROR: Payload for \'get_first_level_children\' invalid: {}".format(e))
    try:
        children = BOM_MANAGER.get_first_level_children(target_part.part)
    except Exception as e:
        raise Exception("Unable to get children: {}".format(e))
    return jsonify(children)


@app.route('/api/get_children', methods=['GET'])
def get_children():
    try:
        payload = request.get_json()
        if payload:
            # User wants children of specific part
            target_part = verify_payload_existing(payload)
            if not target_part:
                raise Exception("ERROR: Requested Part does not exist")
            children_of_target = BOM_MANAGER.get_all_children(target_part.part)
            return jsonify(children_of_target)
    except Exception as e:
        raise Exception("ERROR: Payload for \'get_children\' invalid: {}".format(e))


@app.route('/api/find_parent_assembly', methods=['GET'])
def find_parent_assembly():
    try:
        payload = request.get_json()
        if not payload:
            raise Exception("ERROR: Missing request part in data")
        target_part = verify_payload_existing(payload)
        if not target_part:
            raise Exception("ERROR: Requested Part does not exist")
        parent_assembly = BOM_MANAGER.find_parent_assembly(target_part.part)
        return jsonify(parent_assembly)
    except Exception as e:
        raise Exception("ERROR: Payload for \'find_parent_assembly\' invalid: {}".format(e))


if __name__ == '__main__':
    app.run()
