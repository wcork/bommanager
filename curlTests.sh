#!/usr/bin/env bash
# Create parts
PEN_BLACK_AL="$(curl -s -d '{"name": "Pen", "attributes": {"color": "black", "material": "aluminum"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BLACK_PL="$(curl -s -d '{"name": "Pen", "attributes": {"color": "black", "material": "plastic"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BLUE_AL="$(curl -s -d '{"name": "Pen", "attributes": {"color": "blue", "material": "aluminum"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BLUE_PL="$(curl -s -d '{"name": "Pen", "attributes": {"color": "blue", "material": "plastic"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

# Pen top parts and assemblies
PEN_TOP_AL="$(curl -s -d '{"name": "pen_barrel_top", "attributes": {"material": "aluminum"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOT_BLACK_AL="$(curl -s -d '{"name": "pen_barrel_bot", "attributes": {"material": "aluminum", "color": "black"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOT_BLUE_AL="$(curl -s -d '{"name": "pen_barrel_bot", "attributes": {"material": "aluminum", "color": "blue"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

PEN_TOP_PL="$(curl -s -d '{"name": "pen_barrel_top", "attributes": {"material": "plastic"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOT_BLACK_PL="$(curl -s -d '{"name": "pen_barrel_bot", "attributes": {"material": "plastic", "color": "black"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOT_BLUE_PL="$(curl -s -d '{"name": "pen_barrel_bot", "attributes": {"material": "plastic", "color": "blue"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

POCKET_CLIP="$(curl -s -d '{"name": "pocket_clip", "attributes": {"material": "aluminum"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

# Assemble Pen Tops
echo "$(curl -s -d '{"parent": '$PEN_TOP_AL', "children": ['$POCKET_CLIP'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_TOP_PL', "children": ['$POCKET_CLIP'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"

# Other parts
THRUSTER="$(curl -s -d '{"name": "thruster", "attributes": {"material": "plastic"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
CAM="$(curl -s -d '{"name": "cam", "attributes": {"material": "plastic"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
EXTERNAL_GRIP="$(curl -s -d '{"name": "grip_external", "attributes": {"material": "rubber"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
INTEGRATED_GRIP="$(curl -s -d '{"name": "grip_integrated", "attributes": {"material": "rubber"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
SPRING="$(curl -s -d '{"name": "spring"}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

# Ink and Ink cartridges
INK_CARTRIDGE_BLK="$(curl -s -d '{"name": "ink_cartridge_black"}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
INK_CARTRIDGE_BLU="$(curl -s -d '{"name": "ink_cartridge_blue"}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
INK_CARTRIDGE_BDY="$(curl -s -d '{"name": "ink_cartridge_body", "attributes": {"material": "plastic"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
INK_CARTRIDGE_CAP="$(curl -s -d '{"name": "ink_cartridge_cap", "attributes": {"material": "plastic"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
INK_CARTRIDGE_TIP="$(curl -s -d '{"name": "ink_cartridge_tip"}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

INK_BLK="$(curl -s -d '{"name": "ink", "attributes": {"color": "black"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
INK_BLU="$(curl -s -d '{"name": "ink", "attributes": {"color": "blue"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

# Assemble Ink cartridges
echo "$(curl -s -d '{"parent": '$INK_CARTRIDGE_BLK', "children": ['$INK_CARTRIDGE_BDY','$INK_CARTRIDGE_CAP','$INK_CARTRIDGE_TIP','$INK_BLK'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$INK_CARTRIDGE_BLU', "children": ['$INK_CARTRIDGE_BDY','$INK_CARTRIDGE_CAP','$INK_CARTRIDGE_TIP','$INK_BLU'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"

# Assemble the pens
echo "$(curl -s -d '{"parent": '$PEN_BLACK_AL', "children": ['$PEN_TOP_AL','$PEN_BOT_BLACK_AL','$INK_CARTRIDGE_BLK','$THRUSTER','$CAM','$INTEGRATED_GRIP','$SPRING'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_BLACK_PL', "children": ['$PEN_TOP_PL','$PEN_BOT_BLACK_PL','$INK_CARTRIDGE_BLK','$THRUSTER','$CAM','$INTEGRATED_GRIP','$SPRING'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_BLUE_AL', "children": ['$PEN_TOP_AL','$PEN_BOT_BLUE_AL','$INK_CARTRIDGE_BLU','$THRUSTER','$CAM','$INTEGRATED_GRIP','$SPRING'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_BLUE_PL', "children": ['$PEN_TOP_PL','$PEN_BOT_BLUE_PL','$INK_CARTRIDGE_BLU','$THRUSTER','$CAM','$INTEGRATED_GRIP','$SPRING'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"

# Create the box parts and packaging
PEN_BOX_BLK_AL="$(curl -s -d '{"name": "pen_box", "attributes": {"color": "black", "package": "Aluminum Pen"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOX_BLK_PL="$(curl -s -d '{"name": "pen_box", "attributes": {"color": "black", "package": "Plastic Pen"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOX_BLU_AL="$(curl -s -d '{"name": "pen_box", "attributes": {"color": "blue", "package": "Aluminum Pen"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOX_BLU_PL="$(curl -s -d '{"name": "pen_box", "attributes": {"color": "blue", "package": "Plastic Pen"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

PEN_BOX_TOP_BLK="$(curl -s -d '{"name": "pen_box_top", "attributes": {"color": "black"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOX_TOP_BLU="$(curl -s -d '{"name": "pen_box_top", "attributes": {"color": "blue"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOX_BOT_BLK="$(curl -s -d '{"name": "pen_box_bottom", "attributes": {"color": "black"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"
PEN_BOX_BOT_BLU="$(curl -s -d '{"name": "pen_box_bottom", "attributes": {"color": "blue"}}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

PEN_BOX_INSERT="$(curl -s -d '{"name": "pen_box_insert"}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_part)"

# Assemble boxes
echo "$(curl -s -d '{"parent": '$PEN_BOX_BLK_AL', "children": ['$PEN_BOX_TOP_BLK','$PEN_BOX_BOT_BLK','$PEN_BOX_INSERT'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_BOX_BLK_PL', "children": ['$PEN_BOX_TOP_BLK','$PEN_BOX_BOT_BLK','$PEN_BOX_INSERT'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_BOX_BLU_AL', "children": ['$PEN_BOX_TOP_BLU','$PEN_BOX_BOT_BLU','$PEN_BOX_INSERT'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_BOX_BLU_PL', "children": ['$PEN_BOX_TOP_BLU','$PEN_BOX_BOT_BLU','$PEN_BOX_INSERT'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"

# For sake of final assembly, place the pens in their boxes
echo "$(curl -s -d '{"parent": '$PEN_BOX_BLK_AL', "children": ['$PEN_BLACK_AL'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_BOX_BLK_PL', "children": ['$PEN_BLACK_PL'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_BOX_BLU_AL', "children": ['$PEN_BLUE_AL'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"
echo "$(curl -s -d '{"parent": '$PEN_BOX_BLU_PL', "children": ['$PEN_BLUE_PL'] }' -H "Content-Type: application/json" -X POST http://localhost:5000/api/add_child_parts)"



# Echo show output of each pen box
echo "$(curl -s -d $PEN_BOX_BLK_AL -H "Content-Type: application/json" -X GET http://localhost:5000/api/get_children | json_pp)"
echo "$(curl -s -d $PEN_BOX_BLK_PL -H "Content-Type: application/json" -X GET http://localhost:5000/api/get_children | json_pp)"
echo "$(curl -s -d $PEN_BOX_BLU_AL -H "Content-Type: application/json" -X GET http://localhost:5000/api/get_children | json_pp)"
echo "$(curl -s -d $PEN_BOX_BLU_PL -H "Content-Type: application/json" -X GET http://localhost:5000/api/get_children | json_pp)"